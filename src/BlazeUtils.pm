#!/usr/bin/env perl

package BlazeUtils;

# Blaze-utils - collection of common utilites
# Copyright (C) 2008-2011 Jaromir Hradilek
# Copyright (c) 2015 Peter Gervai

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Digest::MD5;
use File::Basename;
use File::Copy;
use File::Path;
use File::Spec::Functions;
use Getopt::Long;
use Term::ANSIColor;
use Text::Wrap;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw( add_files add_to_log check_header choose_id
	collect_headers collect_ids compare_records date_to_string
	display_record display_version display_warning exit_with_error 
	make_record 
	make_url read_conf read_ini rfc_822_date save_record 
	write_conf write_ini
	$conf $BOM
	$blogdir $editor $process $verbose $compact $coloured
	$reverse $number $force $prompt
	$chosen $reserved);

# General script information:
use constant NAME    => basename($0, '.pm');        # Script name.
use constant VERSION => '1.2.0';                    # Script version.

# General script settings:
our $blogdir  = '.';                                # Repository location.
our $editor   = '';                                 # Editor to use.
our $process  = 1;                                  # Use processor?
our $verbose  = 1;                                  # Verbosity level.
our $compact    = 0;                                # Use compact listing?
our $coloured   = undef;                            # Use colors?
our $reverse    = 0;                                # Use reverse order?
our $number     = 0;                                # Listed records limit.
our $force   = 0;                                   # Force raw file?
our $prompt  = 0;                                   # Ask for confirmation?

# Global variables:
our $chosen   = 1;                                  # Available ID guess.
our $reserved = undef;                              # Reserved ID list.
our $conf     = {};                                 # Configuration.
our $BOM = chr(0xef) . chr(0xbb) . chr(0xbf);	    # UTF-8 ByteOrderMark
						    # to force UTF-8 in the editor

# Set up the __WARN__ signal handler:
$SIG{__WARN__} = sub {
  print STDERR NAME . ": " . (shift);
};

# Display an error message, and terminate the script:
sub exit_with_error {
  my $message      = shift || 'An error has occurred.';
  my $return_value = shift || 1;

  # Display the error message:
  print STDERR NAME . ": $message\n";

  # Terminate the script:
  exit $return_value;
}

# Display a warning message:
sub display_warning {
  my $message = shift || 'A warning was requested.';

  # Display the warning message:
  print STDERR "$message\n";

  # Return success:
  return 1;
}

# Display version information:
sub display_version {
  my ($NAME, $VERSION) = (NAME, VERSION);

  # Display the version:
  print << "END_VERSION";
$NAME $VERSION

Copyright (C) 2008-2011 Jaromir Hradilek
This program is free software; see the source for copying conditions. It is
distributed in the hope  that it will be useful,  but WITHOUT ANY WARRANTY;
without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PAR-
TICULAR PURPOSE.
END_VERSION

  # Return success:
  return 1;
}

# Translate a date to the YYYY-MM-DD [HH:MM] form:
sub date_to_string {
  my @date = localtime(shift);
  my $result = sprintf("%d-%02d-%02d", ($date[5] + 1900), ++$date[4], $date[3]);
  if( $conf->{'post'}->{'usetime'} =~ /^\s*(true|yes)\s*$/i ) {
	  $result .= sprintf(" %02d:%02d", $date[2], $date[1] );
  }
  return $result;
}

# Translate a date to a string in the RFC 822 form:
sub rfc_822_date {
  my @date = localtime(shift);

  # Prepare aliases:
  my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
  my @days   = qw( Sun Mon Tue Wed Thu Fri Sat );

  # Return the result:
  return sprintf("%s, %02d %s %d %02d:%02d:%02d GMT", $days[$date[6]],
                 $date[3], $months[$date[4]], 1900 + $date[5],
                 $date[2], $date[1], $date[0]);
}

# Read data from the INI file:
sub read_ini {
  my $file    = shift || die 'Missing argument';

  # Initialize required variables:
  my $hash    = {};
  my $section = 'default';

  # Open the file for reading:
  open(INI, "$file") or return 0;

  # Process each line:
  while (my $line = <INI>) {
    # Parse the line:
    if ($line =~ /^\s*\[([^\]]+)\]\s*$/) {
      # Change the section:
      $section = $1;
    }
    elsif ($line =~ /^\s*(\S+)\s*=\s*(\S.*)$/) {
      # Add the option to the hash:
      $hash->{$section}->{$1} = $2;
    }
  }

  # Close the file:
  close(INI);

  # Return the result:
  return $hash;
}

# Write data to the INI file:
sub write_ini {
  my $file = shift || 'Missing argument';
  my $hash = shift || 'Missing argument';

  # Open the file for writing:
  open(INI, ">$file") or return 0;

  # Process each section:
  foreach my $section (sort(keys(%$hash))) {
    # Write the section header to the file::
    print INI "[$section]\n";

    # Process each option in the section:
    foreach my $option (sort(keys(%{$hash->{$section}}))) {
      # Write the option and its value to the file:
      print INI "  $option = $hash->{$section}->{$option}\n";
    }
  }

  # Close the file:
  close(INI);

  # Return success:
  return 1;
}

# Read the content of the configuration file:
sub read_conf {
  # Prepare the file name:
  my $file = catfile($blogdir, '.blaze', 'config');

  # Parse the file:
  if (my $conf = read_ini($file)) {
    # Return the result:
    return $conf;
  }
  else {
    # Report failure:
    display_warning("Unable to read the configuration.");

    # Return an empty configuration:
    return {};
  }
}

# Read configuration from the temporary file, and save it:
sub write_conf {
  my $conf = shift || die 'Missing argument';

  # Prepare the file name:
  my $file = catfile($blogdir, '.blaze', 'config');

  # Save the configuration file:
  unless (write_ini($file, $conf)) {
    # Report failure:
    display_warning("Unable to write the configuration.");

    # Return failure:
    return 0;
  }

  # Return success:
  return 1;
}

# Make proper URL from the string while stripping all forbidden characters:
sub make_url {
  my $url = shift || return '';

  # Strip forbidden characters:
  $url =~ s/[^\w\s\-]//g;

  # Strip trailing spaces:
  $url =~ s/\s+$//;

  # Substitute spaces:
  $url =~ s/\s+/-/g;

  # Return the result:
  return $url;
}

# Compose a blog post or a page record:# Display a log entry:
sub display_record {
  my $record = shift || die 'Missing argument';

  # Check whether to use compact listing:
  unless ($compact) {
    # Decompose the record:
    my ($date, $message) = split(/\s+-\s+/, $record, 2);

    # Check whether colors are enabled:
    unless ($coloured) {
      # Display plain record header:
      print "Date: $date\n\n";
    }
    else {
      # Display colored record header:
      print colored ("Date: $date", 'yellow');
      print "\n\n";
    }

    # Display the record body:
    print wrap('    ', '    ', $message);
    print "\n";
  }
  else {
    # Display the short record:
    print $record;
  }

  # Return success:
  return 1;
}

sub make_record {
  my $type = shift || die 'Missing argument';
  my $id   = shift || die 'Missing argument';
  my ($title, $author, $date, $keywords, $tags, $url) = @_;

  # Check whether the title is specified:
  if ($title) {
    # Strip trailing spaces:
    $title =~ s/\s+$//;
  }
  else {
    # Assign the default value:
    $title = 'Untitled';

    # Display the appropriate warning:
    display_warning("Missing title in the $type with ID $id. " .
                    "Using `$title' instead.");
  }

  # Check whether the author is specified:
  unless ($author) {
    # Assign the default value:
    $author = $conf->{user}->{name} || 'admin';

    # Report the missing author:
    display_warning("Missing author in the $type with ID $id. " .
                    "Using `$author' instead.");
  }

  # Check whether the date is specified:
  if ($date) {
    # Check whether the format is valid:
    unless ($date =~ /\d{4}-[01]\d-[0-3]\d/) {
      # Use current date instead:
      $date = date_to_string(time);

      # Report the invalid date:
      display_warning("Invalid date in the $type with ID $id. " .
                      "Using `$date' instead.");
    }
  }
  else {
    # Use current date instead:
    $date = date_to_string(time);

    # Report the missing date:
    display_warning("Missing date in the $type with ID $id. " .
                    "Using `$date' instead.");
  }

  # Check whether the keywords are specified:
  if ($keywords) {
    # Strip quotation marks:
    $keywords =~ s/"//g;
  }

  # Check whether the tags are specified:
  if ($tags) {
    # Make all tags lower case:
    $tags = lc($tags);

    # Strip superfluous spaces:
    $tags =~ s/\s{2,}/ /g;
    $tags =~ s/\s+$//;

    # Strip trailing commas:
    $tags =~ s/^,+|,+$//g;

    # Remove duplicates:
    my %temp = map { $_, 1 } split(/,+\s*/, $tags);
    $tags = join(', ', sort(keys(%temp)));
  }
  else {
    # Assign the default value:
    $tags = '';
  }

  # Check whether the URL is specified:
  if ($url) {
    # Check whether it contains forbidden characters:
    if ($url =~ /[^\w\-]/) {
      # Strip forbidden characters:
      $url = make_url($url);

      # Report the invalid URL:
      display_warning("Invalid URL in the $type with ID $id. " .
                      ($url ? "Stripping to `$url'."
                            : "Deriving from the title."));
    }
  }

  # Unless already created, derive the URL from the blog post or page
  # title:
  unless ($url) {
    # Derive the URL from the blog post or page title:
    $url = make_url(lc($title));
  }

  # Finalize the URL:
  if ($url) {
    # Prepend the ID to the blog post URL:
    $url = "$id-$url" if $type eq 'post';
  }
  else {
    # Base the URL on the ID:
    $url = ($type eq 'post') ? $id : "page$id";

    # Report missing URL:
    display_warning("Empty URL in the $type with ID $id. " .
                    "Using `$url' instead.");
  }

  # Return the composed record:
  return {
    'id'       => $id,
    'title'    => $title,
    'author'   => $author,
    'date'     => $date,
    'keywords' => $keywords,
    'tags'     => $tags,
    'url'      => $url,
  };
}

# Compare two records:
sub compare_records {
  # Check whether to use reverse order:
  unless ($reverse) {
    return sprintf("%s:%08d", $b->{date}, $b->{id}) cmp
           sprintf("%s:%08d", $a->{date}, $a->{id});
  }
  else {
    return sprintf("%s:%08d", $a->{date}, $a->{id}) cmp
           sprintf("%s:%08d", $b->{date}, $b->{id});
  }
}

# Return a list of blog post or page header records:
sub collect_headers {
  my $type    = shift || 'post';

  # Initialize required variables:
  my @records = ();

  # Prepare the file name:
  my $head    = catdir($blogdir, '.blaze', "${type}s", 'head');

  # Open the directory:
  opendir(HEAD, $head) or return @records;

  # Process each file:
  while (my $id = readdir(HEAD)) {
    # Skip both . and ..:
    next if $id =~ /^\.\.?$/;

    # Parse header data:
    my $data     = read_ini(catfile($head, $id)) or next;
    my $title    = $data->{header}->{title};
    my $author   = $data->{header}->{author};
    my $date     = $data->{header}->{date};
    my $keywords = $data->{header}->{keywords};
    my $tags     = $data->{header}->{tags};
    my $url      = $data->{header}->{url};

    # Create the record:
    my $record = make_record($type, $id, $title, $author, $date,
                             $keywords, $tags, $url);

    # Add the record to the beginning of the list:
    push(@records, $record);
  }

  # Close the directory:
  closedir(HEAD);

  # Return the result:
  if ($type eq 'post') {
    return sort {
      sprintf("%s:%08d", $b->{date}, $b->{id}) cmp
      sprintf("%s:%08d", $a->{date}, $a->{id})
    } @records;
  }
  else {
    return sort {
      sprintf("%s:%08d", $a->{date}, $a->{id}) cmp
      sprintf("%s:%08d", $b->{date}, $b->{id})
    } @records;
  }
}

# Look for erroneous or missing header data:
sub check_header {
  my $data = shift || die 'Missing argument';
  my $id   = shift || die 'Missing argument';
  my $type = shift || die 'Missing argument';

  # Check whether the title is specified:
  unless ($data->{header}->{title}) {
    # Display the appropriate warning:
    display_warning("Missing title in the $type with ID $id.");
  }

  # Check whether the author is specified:
  unless ($data->{header}->{author} || $type eq 'page') {
    # Report the missing author:
    display_warning("Missing author in the $type with ID $id.");
  }

  # Check whether the date is specified:
  if (my $date = $data->{header}->{date}) {
    # Check whether the format is valid:
    unless ($date =~ /\d{4}-[01]\d-[0-3]\d/) {
      # Report the invalid date:
      display_warning("Invalid date in the $type with ID $id.");
    }
  }
  else {
    # Report the missing date:
    display_warning("Missing date in the $type with ID $id.");
  }

  # Check whether the tags are specified:
  if (my $tags = $data->{header}->{tags}) {
    # Make all tags lower case:
    $tags = lc($tags);

    # Strip superfluous characters:
    $tags =~ s/\s{2,}/ /g;
    $tags =~ s/\s+$//;
    $tags =~ s/^,+|,+$//g;

    # Remove duplicates:
    my %temp = map { $_, 1 } split(/,+\s*/, $tags);

    # Make sure none of the tags will have an empty URL:
    foreach my $tag (keys %temp) {
      # Derive the URL from the tag name:
      my $tag_url = make_url($tag);

      # Make sure the result is not empty:
      unless ($tag_url) {
        # Report the missing tag URL:
        display_warning("Unable to derive the URL from the tag `$tag'. " .
                        "Please use ASCII characters only.");
      }
    }
  }

  # Check whether the URL is specified:
  if (my $url = $data->{header}->{url}) {
    # Check whether it contains forbidden characters:
    if ($url =~ /[^\w\-]/) {
      # Report the invalid URL:
      display_warning("Invalid URL in the $type with ID $id." .
                      ($url ? "" : " It will be derived from the title."));
    }
  }

  # Make sure the URL can be derived from the title if necessary:
  unless ($data->{header}->{url}) {
    # Derive the URL from the post or page title:
    my $title = $data->{header}->{title} || '';
    my $url   = make_url(lc($title));

    # Check whether the URL is not empty:
    unless ($url) {
      # Report the missing URL:
      display_warning("Unable to derive the URL in the $type with ID $id. " .
                      "Please specify it yourself.");
    }
  }

  # Return success:
  return 1;
}

# Create a record from a single file:
sub save_record {
  my $file = shift || die 'Missing argument';
  my $id   = shift || die 'Missing argument';
  my $type = shift || 'post';
  my $data = shift || {};

  # Initialize required variables:
  my $line = '';

  # Prepare the record directory names:
  my $head_dir  = catdir($blogdir, '.blaze', "${type}s", 'head');
  my $body_dir  = catdir($blogdir, '.blaze', "${type}s", 'body');
  my $raw_dir   = catdir($blogdir, '.blaze', "${type}s", 'raw');

  # Prepare the record file names:
  my $head      = catfile($head_dir, $id);
  my $body      = catfile($body_dir, $id);
  my $raw       = catfile($raw_dir,  $id);

  # Prepare the temporary file names:
  my $temp_head = catfile($blogdir, '.blaze', 'temp.head');
  my $temp_body = catfile($blogdir, '.blaze', 'temp.body');
  my $temp_raw  = catfile($blogdir, '.blaze', 'temp.raw');

  # Read required data from the configuration:
  my $processor = $conf->{core}->{processor};

  # Check whether the processor is enabled:
  if ($process) {
    # Substitute placeholders with actual file names:
    $processor  =~ s/%in%/$temp_raw/ig;
    $processor  =~ s/%out%/$temp_body/ig;
  }

  # Open the input file for reading:
  open(FIN, "$file") or return 0;

  # Parse the file header:
  while ($line = <FIN>) {
    # The header ends with the first line not beginning with "#":
    last unless $line =~ /^$BOM?#/;

    # Collect data for the record header:
    if ($line =~ /(title|author|date|keywords|tags|url):\s*(\S.*)$/) {
      $data->{header}->{$1} = $2;
    }
  }

  # Look for erroneous or missing header data:
  check_header($data, $id, $type);

  # Write the record header to the temporary file:
  write_ini($temp_head, $data) or return 0;

  # Open the proper output file:
  open(FOUT, '>' . ($process ? $temp_raw : $temp_body)) or return 0;

  # Write the last read line to the output file:
  print FOUT $line if $line;

  # Add the rest of the file content to the output file:
  while ($line = <FIN>) {
    print FOUT $line;
  }

  # Close all opened files:
  close(FIN);
  close(FOUT);

  # Check whether the processor is enabled:
  if ($process) {
    # Process the raw input file:
    unless (system("$processor") == 0) {
      # Report failure and exit:
      exit_with_error("Unable to run `$processor'.", 1);
    }

    # Make sure the raw record directory exists:
    unless (-d $raw_dir) {
      # Create the target directory tree:
      eval { mkpath($raw_dir, 0); };

      # Make sure the directory creation was successful:
      exit_with_error("Creating the directory tree: $@", 13) if $@;
    }

    # Create the raw record file:
    move($temp_raw, $raw) or return 0;
  }

  # Make sure the record body and header directories exist:
  unless (-d $head_dir && -d $body_dir) {
    # Create the target directory tree:
    eval { mkpath([$head_dir, $body_dir], 0); };

    # Make sure the directory creation was successful:
    exit_with_error("Creating the directory tree: $@", 13) if $@;
  }

  # Create the record body and header files:
  move($temp_body, $body) or return 0;
  move($temp_head, $head) or return 0;

  # Return success:
  return 1;
}

# Collect reserved post or page IDs:
sub collect_ids {
  my $type = shift || 'post';

  # Prepare the post or page directory name:
  my $head = catdir($blogdir, '.blaze', "${type}s", 'head');

  # Open the header directory:
  opendir(HEADS, $head) or return 0;

  # Build a list of used IDs:
  my @used = grep {! /^\.\.?$/ } readdir(HEADS);

  # Close the directory:
  closedir(HEADS);

  # Return the sorted result:
  return sort {$a <=> $b} @used;
}

# Return the first unused ID:
sub choose_id {
  my $type   = shift || 'post';

  # Get the list of reserved IDs unless already done:
  @$reserved = collect_ids($type) unless defined $reserved;

  # Iterate through the used IDs:
  while (my $used = shift(@$reserved)) {
    # Check whether the candidate ID is really free:
    if ($chosen == $used) {
      # Try the next ID:
      $chosen++;
    }
    else {
      # Push the last checked ID back to the list:
      unshift(@$reserved, $used);

      # Exit the loop:
      last;
    }
  }

  # Return the result, and increase the next candidate number:
  return $chosen++;
}

# Add given files to the repository:
sub add_files {
  my $type  = shift || 'post';
  my $data  = shift || {};
  my $files = shift || die 'Missing argument';

  # Initialize required variables:
  my @list  = ();

  # Process each file:
  foreach my $file (@{$files}) {
    # Get the first available ID:
    my $id = choose_id($type);

    # Save the record:
    save_record($file, $id, $type, $data)
      and push(@list, $id)
      or display_warning("Unable to add $file.");
  }

  # Return the list of added IDs:
  return @list;
}

# Add the event to the log:
sub add_to_log {
  my $text = shift || 'Something miraculous has just happened!';

  # Prepare the log file name:
  my $file = catfile($blogdir, '.blaze', 'log');

  # Open the log file for appending:
  open(LOG, ">>$file") or return 0;

  # Write the event to the file:
  print LOG localtime(time) . " - $text\n";

  # Close the file:
  close(LOG);

  # Return success:
  return 1;
}

1;
