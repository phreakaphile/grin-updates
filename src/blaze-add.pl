#!/usr/bin/env perl

# blaze-add - adds a blog post or a page to a BlazeBlogger repository
# Copyright (C) 2008-2011 Jaromir Hradilek

# This program is  free software:  you can redistribute it and/or modify it
# under  the terms  of the  GNU General Public License  as published by the
# Free Software Foundation, version 3 of the License.
#
# This program  is  distributed  in the hope  that it will  be useful,  but
# WITHOUT  ANY WARRANTY;  without  even the implied  warranty of MERCHANTA-
# BILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the  GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use Digest::MD5;
use File::Basename;
use File::Copy;
use File::Path;
use File::Spec::Functions;
use Getopt::Long;

use lib dirname( __FILE__ );
use BlazeUtils;

# General script information:
use constant NAME    => basename($0, '.pl');        # Script name.
use constant VERSION => '1.2.0';                    # Script version.

# Set up the __WARN__ signal handler:
$SIG{__WARN__} = sub {
  print STDERR NAME . ": " . (shift);
};

# Display usage information:
sub display_help {
  my $NAME = NAME;

  # Display the usage:
  print << "END_HELP";
Usage: $NAME [-pqCPV] [-b DIRECTORY] [-E EDITOR] [-a AUTHOR] [-d DATE]
                 [-t TITLE] [-k KEYWORDS] [-T TAGS] [-u URL] [FILE...]
       $NAME -h|-v

  -b, --blogdir DIRECTORY     specify a directory in which the BlazeBlogger
                              repository is placed
  -E, --editor EDITOR         specify an external text editor
  -t, --title TITLE           specify a title
  -a, --author AUTHOR         specify an author
  -d, --date DATE             specify a date of publishing
  -k, --keywords KEYWORDS     specify a comma-separated list of keywords
  -T, --tags TAGS             specify a comma-separated list of tags
  -u, --url URL               specify a URL
  -p, --page                  add a page or pages
  -P, --post                  add a blog post or blog posts
  -C, --no-processor          disable processing the blog post or page with
                              an external application
  -q, --quiet                 do not display unnecessary messages
  -V, --verbose               display all messages
  -h, --help                  display this help and exit
  -v, --version               display version information and exit
END_HELP

  # Return success:
  return 1;
}


# Set default options:
my $type  = 'post';                                 # Type: post or page.
my $added = '';                                     # List of added IDs.
my $data  = {};                                     # Post/page metadata.

# Add a new record to the repository:
sub add_new {
  my $type = shift || 'post';
  my $data = shift || {};

  # Decide which editor to use:
  my $edit = $editor || $conf->{core}->{editor} || $ENV{EDITOR} || 'vi';

  # Prepare the data for the temporary file header:
  my $title    = $data->{header}->{title}    || '';
  my $author   = $data->{header}->{author}   || $conf->{user}->{nickname}
                                             || $conf->{user}->{name}
                                             || 'admin';
  my $date     = $data->{header}->{date}     || date_to_string(time);
  my $keywords = $data->{header}->{keywords} || '';
  my $tags     = $data->{header}->{tags}     || '';
  my $url      = $data->{header}->{url}      || '';

  # Declare other necessary variables:
  my $head;

  # Prepare the temporary file header:
  if ($type eq 'post') {
    # Use the variant for a blog post:
    $head = << "END_POST_HEADER";
$BOM# This and the following lines beginning with '#' are the blog post header.
# Please take your time and replace these options with desired values. Just
# remember that the date has to be in the YYYY-MM-DD form, tags are a comma
# separated list of categories the post (pages ignore these) belong to, and
# the url,  if provided, should consist of alphanumeric characters, hyphens
# and underscores only.  Specifying your own url  is especially recommended
# in case you use non-ASCII characters in your blog post title.
#
#   title:    $title
#   author:   $author
#   date:     $date
#   keywords: $keywords
#   tags:     $tags
#   url:      $url
#
# The header ends here. The rest is the content of your blog post.

END_POST_HEADER
  }
  else {
    # Use the variant for a page:
    $head = << "END_PAGE_HEADER";
$BOM# This and the following lines beginning with '#' are the page header. Ple-
# ase take your time and replace these  options with  desired  values. Just
# remember that the date has to be in the YYYY-MM-DD form, and the  url, if
# provided, should  consist of alphanumeric characters,  hyphens and under-
# scores only. Specifying your own url  is especially  recommended  in case
# you use non-ASCII characters in your page title.
#
#   title:    $title
#   author:   $author
#   date:     $date
#   keywords: $keywords
#   url:      $url
#
# The header ends here. The rest is the content of your page.

END_PAGE_HEADER
  }

  # Prepare the temporary file name:
  my $temp = catfile($blogdir, '.blaze', 'temp');

  # Open the file for writing:
  if (open(FILE, ">$temp")) {
    # Write the temporary file:
    print FILE $head;

    # Close the file:
    close(FILE);
  }
  else {
    # Report failure:
    display_warning("Unable to create the temporary file.");

    # Return failure:
    return 0;
  }

  # Open the temporary file in the external editor:
  unless (system("$edit $temp") == 0) {
    # Report failure and exit:
    exit_with_error("Unable to run `$edit'.", 1);
  }

  # Open the file for reading:
  if (open(FILE, "$temp")) {
    # Set the input/output handler to "binmode":
    binmode(FILE);

    # Count the checksums:
    my $before = Digest::MD5->new->add($head)->hexdigest;
    my $after  = Digest::MD5->new->addfile(*FILE)->hexdigest;

    # Close the file:
    close(FILE);

    # Compare the checksums:
    if ($before eq $after) {
      # Report abortion:
      display_warning("The file has not been changed: aborting.");

      # Return success:
      exit 0;
    }
  }

  # Add the file to the repository:
  my @list = add_files($type, $data, [ $temp ]);

  # Remove the temporary file:
  unlink $temp;

  # Return the record ID:
  return shift(@list);
}

# Set up the option parser:
Getopt::Long::Configure('no_auto_abbrev', 'no_ignore_case', 'bundling');

# Process command line options:
GetOptions(
  'help|h'         => sub { display_help();    exit 0; },
  'version|v'      => sub { display_version(); exit 0; },
  'page|pages|p'   => sub { $type    = 'page'; },
  'post|posts|P'   => sub { $type    = 'post'; },
  'no-processor|C' => sub { $process = 0;      },
  'quiet|q'        => sub { $verbose = 0;      },
  'verbose|V'      => sub { $verbose = 1;      },
  'blogdir|b=s'    => sub { $blogdir = $_[1];  },
  'editor|E=s'     => sub { $editor  = $_[1];  },
  'title|t=s'      => sub { $data->{header}->{title}    = $_[1]; },
  'author|a=s'     => sub { $data->{header}->{author}   = $_[1]; },
  'date|d=s'       => sub { $data->{header}->{date}     = $_[1]; },
  'keywords|k=s'   => sub { $data->{header}->{keywords} = $_[1]; },
  'tags|tag|T=s'   => sub { $data->{header}->{tags}     = $_[1]; },
  'url|u=s'        => sub { $data->{header}->{url}      = $_[1]; },
);

# Check whether the repository is present, no matter how naive this method
# actually is:
exit_with_error("Not a BlazeBlogger repository! Try `blaze-init' first.",1)
  unless (-d catdir($blogdir, '.blaze'));

# Read the configuration file:
$conf = read_conf();

# Check whether the processor is enabled in the configuration:
if ($process && (my $processor = $conf->{core}->{processor})) {
  # Make sure the processor specification is valid:
  exit_with_error("Invalid core.processor option.", 1)
    unless ($processor =~ /%in%/i && $processor =~ /%out%/i);
}
else {
  # Disable the processor:
  $process = 0;
}

# Check whether a file is supplied:
if (scalar(@ARGV) == 0) {
  # Add a new record to the repository:
  $added   = add_new($type, $data)
    or exit_with_error("Cannot add the $type to the repository.", 13);
}
else {
  # Add given files to the repository:
  my @list = add_files($type, $data, \@ARGV)
    or exit_with_error("Cannot add the ${type}s to the repository.", 13);

  # Prepare the list of successfully added IDs:
  $added   =  join(', ', sort(@list));
  $added   =~ s/, ([^,]+)$/ and $1/;
}

# Log the event:
add_to_log("Added the $type with ID $added.")
  or display_warning("Unable to log the event.");

# Report success:
print "Successfully added the $type with ID $added.\n" if $verbose;

# Return success:
exit 0;

__END__

=head1 NAME

blaze-add - adds a blog post or a page to a BlazeBlogger repository

=head1 SYNOPSIS

B<blaze-add> [B<-pqCPV>] [B<-b> I<directory>] [B<-E> I<editor>]
[B<-a> I<author>] [B<-d> I<date>] [B<-t> I<title>] [B<-k> I<keywords>]
[B<-T> I<tags>] [B<-u> I<url>] [I<file>...]

B<blaze-add> B<-h>|B<-v>

=head1 DESCRIPTION

B<blaze-add> adds a blog post or a page to a BlazeBlogger repository. If a
I<file> is supplied, B<blaze-add> adds the content of that file, otherwise
it opens a text editor for you. Note that there are several special forms
and placeholders that can be used in the text and that will be replaced
with a proper data when the blog is generated.

=head2 Special Forms

=over

=item B<< <!-- break --> >>

A mark to delimit a blog post synopsis.

=back

=head2 Placeholders

=over

=item B<%root%>

A relative path to the root directory of the blog.

=item B<%home%>

A relative path to the index page of the blog.

=item B<%page[>I<id>B<]%>

A relative path to a page with the supplied I<id>.

=item B<%post[>I<id>B<]%>

A relative path to a blog post with the supplied I<id>.

=item B<%tag[>I<name>B<]%>

A relative path to a tag with the supplied I<name>.

=back

=head1 OPTIONS

=over

=item B<-b> I<directory>, B<--blogdir> I<directory>

Allows you to specify a I<directory> in which the BlazeBlogger repository
is placed. The default option is a current working directory.

=item B<-E> I<editor>, B<--editor> I<editor>

Allows you to specify an external text I<editor>. When supplied, this
option overrides the relevant configuration option.

=item B<-t> I<title>, B<--title> I<title>

Allows you to specify the I<title> of a blog post or page.

=item B<-a> I<author>, B<--author> I<author>

Allows you to specify the I<author> of a blog post or page.

=item B<-d> I<date>, B<--date> I<date>

Allows you to specify the I<date> of publishing of a blog post or page.

=item B<-k> I<keywords>, B<--keywords> I<keywords>

Allows you to specify a comma-separated list of I<keywords> attached to
a blog post or page.

=item B<-T> I<tags>, B<--tags> I<tags>

Allows you to supply a comma-separated list of I<tags> attached to a blog
post.

=item B<-u> I<url>, B<--url> I<url>

Allows you to specify the I<url> of a blog post or page. Allowed characters
are letters, numbers, hyphens, and underscores.

=item B<-p>, B<--page>, B<--pages>

Tells B<blaze-add> to add a page or pages.

=item B<-P>, B<--post>, B<--posts>

Tells B<blaze-add> to add a blog post or blog posts. This is the default
option.

=item B<-C>, B<--no-processor>

Disables processing a blog post or page with an external application. For
example, if you use Markdown to convert the lightweight markup language to
the valid HTML output, this will enable you to write this particular post
in plain HTML directly.

=item B<-q>, B<--quiet>

Disables displaying of unnecessary messages.

=item B<-V>, B<--verbose>

Enables displaying of all messages. This is the default option.

=item B<-h>, B<--help>

Displays usage information and exits.

=item B<-v>, B<--version>

Displays version information and exits.

=back

=head1 ENVIRONMENT

=over

=item B<EDITOR>

Unless the B<core.editor> option is set, BlazeBlogger tries to use
system-wide settings to decide which editor to use.

=back

=head1 EXAMPLE USAGE

Write a new blog post in an external text editor:

  ~]$ blaze-add

Add a new blog post from a file:

  ~]$ blaze-add new_packages.txt
  Successfully added the post with ID 10.

Write a new page in an external text editor:

  ~]$ blaze-add -p

Write a new page in B<nano>:

  ~]$ blaze-add -p -E nano

=head1 SEE ALSO

B<blaze-init>(1), B<blaze-config>(1), B<blaze-edit>(1), B<blaze-remove>(1),
B<blaze-make>(1)

=head1 BUGS

To report a bug or to send a patch, please, add a new issue to the bug
tracker at <http://code.google.com/p/blazeblogger/issues/>, or visit the
discussion group at <http://groups.google.com/group/blazeblogger/>.

=head1 COPYRIGHT

Copyright (C) 2008-2011 Jaromir Hradilek

This program is free software; see the source for copying conditions. It is
distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.

=cut
